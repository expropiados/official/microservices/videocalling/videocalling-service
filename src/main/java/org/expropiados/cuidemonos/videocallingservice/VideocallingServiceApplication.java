package org.expropiados.cuidemonos.videocallingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideocallingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideocallingServiceApplication.class, args);
    }

}
